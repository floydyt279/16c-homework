#include <iostream>
#include <time.h>

const int N = 10;



int main()
{

	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);

	int arr[N][N];

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			arr[i][j] = i + j;
		}
	}

	std::cout << "Array:" << std::endl;
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			std::cout << arr[i][j] << " ";
		}
		std::cout << std::endl;
	}



	int sum = 0;
	int row_index = buf.tm_mday % N;

	for (int j = 0; j < N; j++)
	{
		sum += arr[row_index][j];
	}
	std::cout << "Sum of elements int row " << row_index << ": " << sum << std::endl;

	
}
